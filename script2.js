const firstName = 'anwar';
let lastname = 'raharja';

const status = true;

const word = "This is string"; // string
const num = 123; //num
const isBool = true; //boolean
const isNull = null; //null
const isUndefined = undefined; // undefined
const car = {
    wheels: 4,
    color: 'Red',
    isNew: false,
};
const cars = ['BMW', "Honda", 'Toyota'];
const cars2 = [{
    wheels: 4,
    color: 'Red',
    isNew: false,
}, {
    wheels: 4,
    color: 'Red',
    isNew: true,
}, {
    wheels: 4,
    color: 'Red',
    isNew: false,
}];
cars2.map((car) => {
    //console.log(car.color)
});

const students = [
    {
        name: "Ersaad",
        score: 90,
        age: 10,
    },
    {
        name: "Gisda",
        score: 82,
        age: 11,
    },
    {
        name: "Anwar",
        score: 76,
        age: 11,
    },
    {
        name: "Jo",
        score: 80,
        age: 11,
    },
    {
        name: "Eka",
        score: 75,
        age: 12,
    },
    {
        name: "Dody",
        score: 78,
        age: 12,
    },
    {
        name: "Putera",
        score: 88,
        age: 10,
    },
    {
        name: "Esya",
        score: 98,
        age: 10,
    },
];
const x = 3;
const y = 4;
const result = y > 3 ? true : false;

const { wheels, isNew, color } = car;
const [firstData, secondData] = students;

const filteredData = cars.filter((car) => car.isNew === true);
const findData = cars2.find((car) => car.isNew == false);
//console.log(cars2);
//console.log(filteredData);
//console.log(findData);
const findPutera = students.find((student) => student.name === 'Putera');
console.log(findPutera);
const filterScore = students.filter((student) => student.score > 80 && student.age > 11);
console.log(filterScore);

const a = '10';
const b = 10;

const samadua = a == b;
const samatiga = a === b;
console.log(samadua);
console.log(samatiga);

if (status) {
    const firstName = 'anwar raharja';
    //console.log(firstName);
}

//function add(){
const add = (x, y) => {
    const result = x + y;
    return result;
}

console.log(add(10, 12));

const luasPersegi = (x) => {
    const result = x * x;
    return result;
}

const luasSegitiga = (x, y) => {
    const result = (x * y) / 2;
    return result;
}

const introduce = (student) => {
    const { name, score, age } = student;
    const word = `Hello my name is ${name}, my age is ${age}, my score is ${score}`;
    return word;
}

console.log("luasPersegi(3)=" + luasPersegi(3));
console.log("luasSegitiga(4,4)=" + luasSegitiga(4, 4));
console.log("luasSegitiga(4,10)=" + luasSegitiga(4, 10));

console.log(introduce(students[0]));
const solution = (x, y) => {

    if (x % 5 === 0) {
        const result = y - x - 0.5;
        if (result > 0)
            return result;
        else
            return "Saldo Kurang";
    }
    return y;
}
console.log("solution(30, 20)= " + solution(30, 20));
console.log("solution(42, 100)= " + solution(42, 100));
console.log("solution(40, 40)= " + solution(40, 40));
console.log("solution(50, 60)= " + solution(50, 60));

const nilai = (x) => {

    if(x >= 91) return "A";
    else if(x >= 81) return "B";
    else if(x >= 71) return "C";
    else if(x >= 61) return "D";
    else return "E";
}
console.log("nilai(95)= " + nilai(95));
console.log("nilai(81)= " + nilai(81));
console.log("nilai(60)= " + nilai(60));
console.log("nilai(78)= " + nilai(78));
